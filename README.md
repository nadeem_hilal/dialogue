# dialogue API v1.0
Basic crud api's for a discussion forum application.<br/>
Api Responeses are only in JSON format.<br/>
Uses Token based Authentication.<br/>
<p align="center"><a href="https://laravel.com" target="_blank"><img width="150"src="https://laravel.com/laravel.png"></a></p>

<p align="center">
<!-- <a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a> -->
<a href="https://styleci.io/repos/118449786"><img src="https://styleci.io/repos/118449786/shield?branch=master" alt="StyleCI"></a>
<!-- <a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a> -->
</p><br/>

## Api Documentation.
### Authenticated endpoints.
|  Httpverb |        ApiEndPoints          |   Method   |         Description              |  Form Inputs Needed |
| --------  |  --------------------------  | ---------- |  ------------------------------- |  ------------------ |
|   GET     |     /questions/              |   Index    |   Returns Questions collections. |       none          |
|   GET     |     /questions/{question}    |   Read     |   Returns a particular Question. |       none          |
|   POST    |     /questions               |   Create   |   Creates a Question.            |   title,content     |
|   DELETE  |     /questions/{question}    |   Delete   |   Deletes an Question.           |    none             |
|   PATCH   |     /questions/{question}    |   UPDATE   |   Update question status.        |    none             |
|   PUT     |     /questions/{question}    |   Edit     |   Edit an Question.              |   content,title     |
|   POST    |     /questions/{question}    |   Create   |   Creates an Answer.             |    content          |
|   PATCH   |     /questions/{questionId}/answers/{answer}  |   Update   |   Update an Answer.             |    content          |
|   DELETE  |     /questions/{questionId}/answers/{answer}  |   Delete   |   Delete an Answer.             |    none             |


### UnAuthenticated endpoints.
  Httpverb |        ApiEndPoints           |   Method   |         Description              |  Form Inputs Needed            |
| -------- |  --------------------------   | ---------- |  ------------------------------- |  ---------------------------   |
|   POST   |     /login/                   |   login    |   Return the loggedin user       |   email,password               |
|   POST    |    /register                 |   Create   |   Creates a new User.            |   email,username,password,name |

# Testing Documentation

## Feature\Answer\DeleteAnswer

*   ✓ User can delete his answer successfully

## Feature\Answer\EditAnswer

*   ✓ User can edit his answer on some question

## Feature\Answer\PublishAnswer

*   ✓ User can publish answer on some question

## Feature\Authentication\LoginUser

*   ✓ User login is successfull when creds match
*   ✓ Show 401 when creds donot match
*   ✓ Show 404 email is required
*   ✓ Show 404 email is not valid
*   ✓ Show 404 password is required
*   ✓ Show 404 when password length is less than 6 characters

## Feature\Authentication\Registration

*   ✓ A guest user can register successfully

## Feature\Profile\EditProfile

*   ✓ User can edit his profile successfully

## Feature\Question\DeleteQuestion

*   ✓ User can delete his question successfully

## Feature\Question\EditQuestion

*   ✓ User can edits his question successfully

## Feature\Question\PublishAQuestion

*   ✓ Publish question succesfully
*   ✓ Show 422 when validation fails on publishing new question

## Feature\Question\UpdateQuestion

*   ✓ User can update his question

## Feature\Question\ViewQuestions

*   ✓ View questions on homepage
*   ✓ View a particular question
*   ✓ The latest question is shown at top
*   ✓ Show 404 when wrong question id is routed

## Feature\User\UserKarma

*   ✓ User will get 10 karma points by default when he registers
*   ✓ Show karma of member
*   ✓ User karma increases when he publishes a new question
*   ✓ User karma decreases when he deletes a question
*   ✓ User karma increases when new answer is published
*   ✓ User karma decreases when answer is deleted

## Unit\Answer\DeleteAnswer

*   ✓ Show 404 when user attempts to delete a deleted answer
*   ✓ Show 404 when user attempts to read a deleted answer
*   ✓ Show 403 when user attempts to delete other users answer

## Unit\Answer\EditAnswer

*   ✓ Show 422 when user attempts to edit his answer on some question and validation fails
*   ✓ Show 403 when user attempts to edit other users answer on some question

## Unit\Answer\PublishAnswer

*   ✓ Show 422 when user attempts to publish answer and validation fails
*   ✓ Show 404 when user attempts to publish answer on question not found in database

## Unit\Authentication\Registration

*   ✓ Show 400 when all validation fails as guest user attempts to register
*   ✓ User name required
*   ✓ User name already taken
*   ✓ User name not string
*   ✓ User name must be atleast 5 character string
*   ✓ Password is required
*   ✓ Password not string
*   ✓ Password must be a string with atleast 6 characters
*   ✓ Email is required
*   ✓ Email is must be valid email
*   ✓ Email already exists
*   ✓ Name is required
*   ✓ Name is must be string
*   ✓ Name must not exceed more then 20 characters

## Unit\Profile\EditProfile

*   ✓ Show 403 when user attempts to edit other user profile
*   ✓ Show 404 when username notfound
*   ✓ Throws 400 when user edit profile and validation fails

## Unit\Question\DeleteQuestion

*   ✓ Show 404 when user deletes question not in database
*   ✓ Show 403 when user attempts to delete other users question

## Unit\Question\EditQuestion

*   ✓ Show 403 when user attempts to edit answered question
*   ✓ Show 404 when user attempts to edit question not in database
*   ✓ Show 400 when user attempts to edit his question and validation fails
*   ✓ Show 403 when user attempts to edit other users question

## Unit\Question\UpdateQuestion

*   ✓ Show 400 when user updates his question and validation fails
*   ✓ Show 404 when user update question not in database
*   ✓ Show 403 when user attempts to update other users question

## Unit\User\UserKarma

*   ✓ Increase karma
*   ✓ Decrease karma

## License
The dialogue is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
