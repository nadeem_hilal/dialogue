<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Answer extends Model
{
    use SoftDeletes;
    public $timestamps = false;
    protected $fillable = ['content', 'id', 'user_id', 'question_id', 'posted_at'];
    protected $dates = ['deleted_at'];

    /**
     * Add relationship i.e Answer belongs to a user.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Adds relationship i.e Answer belongs to a Question.
     */
    public function question()
    {
        return $this->belongsTo('App\Question');
    }
}
