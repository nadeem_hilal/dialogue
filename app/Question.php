<?php

namespace App;

use Auth;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Question extends Model
{
    use SoftDeletes;
    protected $guarded = ['id'];
    protected $fillable = ['title', 'id', 'user_id', 'content', 'posted_at', 'status', 'deleted'];
    protected $dates = ['deleted_at'];
    public $timestamps = false;

    /**
     * Mutates the Status attribute with ist word Capital.
     *
     * @param string $value.
     *
     * @return string $value
     */
    public function getStatusAttribute($value)
    {
        return ucfirst($value);
    }

    /**
     * Mutates the Status Content with line breaks.
     *
     * @param string $value.
     *
     * @return string $value
     */
    public function getContentAttribute($value)
    {
        return nl2br($value);
    }

    /**
     * Add relationship i.e Question belongs to user.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Add relationship i.e Question has many answers.
     */
    public function answers()
    {
        return $this->hasMany('App\Answer');
    }

    /**
     * Create Question .
     *
     * @param Illuminate\Http\Request $request
     */
    public static function publishQuestion($request)
    {
        $question = factory(self::class)->create([
            'title'     => $request['title'],
            'content'   => $request['content'],
            'user_id'   => Auth::id(),
            'posted_at' => Carbon::now(),
        ]);
        User::increaseKarma($question->user, env('NEW_QUESTION'));
    }

    /**
     *  Returns the latest Questions.
     *
     * @return Question
     */
    public static function latest()
    {
        return self::where('deleted_at', null)
            ->orderBy('posted_at', 'desc')
            ->orderBy('id', 'desc');
    }

    public function addAnswer($answer)
    {
        $this->answers()->create($answer);
    }
}
