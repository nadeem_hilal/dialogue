<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'user_name', 'level',
            'joined_at', 'password', 'remember_token', 'api_token', 'karma', ];
    public $timestamps = false;
    protected $guarded = ['id'];
    protected $dates = [
        'joined_at',
        'updated_at',
        'deleted_at',
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'level',
    ];

    public function generateToken()
    {
        $this->remember_token = str_random(60);
        $this->save();

        return $this->remember_token;
    }

    public function questions()
    {
        return $this->hasMany(Question::class);
    }

    public function answers()
    {
        return $this->hasMany('App\Answer');
    }

    public function editProfile($request)
    {
        $this->update($request);
        $this->save();
    }

    public static function increaseKarma($user, $points)
    {
        $user->karma += $points;
        $user->save();
    }

    public static function decreaseKarma($user, $points)
    {
        $user->karma -= $points;
        $user->save();
    }

    public static function findByUsername($username)
    {
        return self::where('user_name', $username)->first();
    }
}
