<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProfilePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine whether the user can update the answer.
     *
     * @param \App\User   $user
     * @param \App\Answer $answer
     *
     * @return mixed
     */
    public function update(User $updater, User $user)
    {
        return $user->api_token === $updater->api_token;
    }
}
