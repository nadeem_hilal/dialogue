<?php

namespace App\Transformers;

class QuestionTransformer extends Transformer
{
    /**
     *  Transforms an array to proper standard format.
     *
     * @param array $question
     *
     * @return array.
     */
    public function transform($question)
    {
        return [
            'id'               => (int) $question->id,
            'title'            => (string) $question->title,
            'body'             => (string) $question->content,
            'author_username'  => (string) $question->user->user_name,
            'author_name'      => (string) $question->user->name,
            'published_at'     => (string) $question->posted_at,
            'status'           => (string) $question->status,
            'deleted'          => (bool) $question->deleted,
        ];
    }

    /**
     * Transforms an Question array with other relationships to proper format.
     *
     * @param array $question
     *
     * @return array.
     */
    public function transformWithAnswers($question)
    {
        return [
            'id'               => (int) $question->id,
            'title'            => (string) $question->title,
            'body'             => (string) $question->content,
            'author_username'  => (string) $question->user->user_name,
            'author_name'      => (string) $question->user->name,
            'published_at'     => (string) $question->posted_at,
            'status'           => (string) $question->status,
            'deleted'          => (bool) $question->deleted,
            'answers'          => $question->answers->map(function ($answer) {
                return [
                    'answer_id'       => (int) $answer->id,
                    'author_username' => (string) $answer->user->user_name,
                    'author_name'     => (string) $answer->user->name,
                    'body'            => (string) $answer->content,
                    'published_at'    => (string) $answer->posted_at,
                ];
            }),
            ];
    }
}
