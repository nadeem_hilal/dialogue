<?php

namespace App\Transformers;

abstract class Transformer
{
    /**
     * Transforms a collection to proper format .
     *
     * @param array $items.
     *
     * @return array.
     */
    public function transformCollection(array $items)
    {
        return array_map([$this, 'transform'], $items);
    }

    /**
     * Transforms to proper format.
     *
     * @param array $item.
     *
     * @return array.
     */
    abstract public function transform($item);
}
