<?php

namespace App\Transformers;

class ProfileTransformer extends Transformer
{
    /**
     *  Transforms an array to proper standard format.
     *
     * @param array $user
     *
     * @return array.
     */
    public function transform($user)
    {
        return [
            'id'       => (int) $user->id,
            'name'     => (string) $user->name,
            'username' => (string) $user->user_name,
            'email'    => (string) $user->email,
            'isAdmin'  => (bool) $user->level,
            'joined'   => (string) $user->joined_at->diffForHumans(),
            'karma'    => (int) $user->karma,
        ];
    }
}
