<?php

namespace App\Transformers;

class AnswerTransformer extends Transformer
{
    /**
     *  Transforms an array to proper format.
     *
     * @param array $answer
     *
     * @return array.
     */
    public function transform($answer)
    {
        return [
                'answer_id'       => (int) $answer->id,
                'author_username' => (string) $answer->user->user_name,
                'author_name'     => (string) $answer->user->name,
                'body'            => (string) $answer->content,
                'published_at'    => (string) $answer->posted_at,
                ];
    }
}
