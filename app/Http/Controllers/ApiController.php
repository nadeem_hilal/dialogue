<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Response;
use Validator;

class ApiController extends Controller
{
    /**
     * @var Http statuscode.
     **/
    protected $statusCode = 200;

    /**
     *  Return the statusCode that was saved on object.
     *
     * @return statusCode.
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     *  Sets the statusCode.
     *
     * @param int $code
     *
     * @return App\Http\Controllers\ApiController.
     */
    public function setStatusCode($code)
    {
        $this->statusCode = $code;

        return $this;
    }

    /**
     * @param string $message
     *
     * @return mixed
     */
    public function respondNotFound($message = 'Not Found')
    {
        return $this->setStatusCode(404)->respondWithError($message);
    }

    /**
     * @param string $message
     *
     * @return mixed
     */
    public function respondWithError($message)
    {
        return $this->respond([
           'errors' => $message,
        ]);
    }

    /**
     * @param array $data
     * @param array $headers
     *
     * @return Response
     */
    public function respond($data, $headers = [])
    {
        return Response::json($data, $this->getStatusCode(), $headers);
    }

    /**
     * @param string $message
     *
     * @return mixed
     */
    public function respondWithSuccess($message)
    {
        return $this->setStatusCode(200)->respond([
            'data' => [
                'message'   => $message,
                'status'    => 'success', ],
        ]);
    }

    /**
     * Get a validator for an incoming request.
     *
     * @param array $data
     * @param array $rules
     */
    public function validator(array $data, array $rules)
    {
        return Validator::make($data, $rules);
    }
}
