<?php

namespace App\Http\Controllers;

use App\Answer;
use App\Http\Requests\PublishAnswerRequest;
use App\Question;
use App\Transformers\AnswerTransformer;
use App\User;
use Carbon\Carbon;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class AnswersController extends ApiController
{
    /**
     * @var App\Transformers\AnswerTransformer
     **/
    protected $answerTransformer;

    public function __construct(AnswerTransformer $answerTransformer)
    {
        $this->answerTransformer = $answerTransformer;
    }

    /**
     * Store Answer.
     *
     * @param App\Question                           $question
     * @param App\Answer                             $answer
     * @param App\Http\Requests\PublishAnswerRequest $request
     *
     * @return mixed.
     */
    public function store(Question $question, Answer $answer, PublishAnswerRequest $request)
    {
        $question->addAnswer([
             'content'  => request('content'),
             'user_id'  => auth()->id(),
             'posted_at'=> Carbon::now(),
        ]);
        User::increaseKarma(auth()->user(), env('NEW_ANSWER'));

        return $this->respondWithSuccess('Answer posted successfully');
    }

    /**
     * Edit Answer.
     *
     * @param App\Question                           $questionId
     * @param App\Answer                             $answer
     * @param App\Http\Requests\PublishAnswerRequest $request
     *
     * @throws Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException
     * @throws Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     *
     * @return mixed.
     */
    public function edit($questionId, Answer $answer, PublishAnswerRequest $request)
    {
        try {
            $this->authorize('update', $answer);
            $answer->update($request->all());

            return $this->respondWithSuccess('Answer updated Successuly');
        } catch (\Exception $e) {
            throw new AccessDeniedHttpException();
        }
    }

    /**
     * Delete Answer.
     *
     * @param App\Question $questionId
     * @param App\Answer   $answer
     *
     * @throws Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException
     * @throws Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     *
     * @return mixed.
     */
    public function delete($questionId, Answer $answer)
    {
        try {
            $this->authorize('delete', $answer);
            User::decreaseKarma($answer->user, env('DELETE_ANSWER'));
            $answer->delete();

            return $this->respondWithSuccess('Answer deleted Successuly');
        } catch (\Exception $e) {
            throw new AccessDeniedHttpException();
        }
    }
}
