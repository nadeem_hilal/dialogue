<?php

namespace App\Http\Controllers;

use App\Http\Requests\EditProfileRequest;
use App\Transformers\ProfileTransformer;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class ProfileController extends ApiController
{
    /**
     * @var App\Transformers\ProfileTransformer
     **/
    protected $profileTransformer;

    public function __construct(ProfileTransformer $profileTransformer)
    {
        $this->profileTransformer = $profileTransformer;
    }

    public function show($user)
    {
        return $this->respond(['data' => $this->profileTransformer->transform($user)]);
    }

    /**
     * Edit the user profile.
     *
     * @param App\User $user
     * @param  App\Http\Request\EditProfileRequest
     *
     * @throws Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(EditProfileRequest $request, $user)
    {
        try {
            $this->authorize('update', $user);

            $user->editProfile($request->all());

            return $this->respondWithSuccess('Profile updated Successuly');
        } catch (\Exception $e) {
            throw new AccessDeniedHttpException();
        }
    }
}
