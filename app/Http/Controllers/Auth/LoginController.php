<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->middleware('guest')->except('logout');
    }

    protected function validateLoginCreds(array $data)
    {
        return Validator::make($data, [
            'email'    => 'required|email',
            'password' => 'required|string|min:6',
        ]);
    }

    public function login(Request $request)
    {
        $validator = $this->validateLoginCreds($request->all());
        if ($validator->fails()) {
            return Response::json(['errors' => $validator->errors()], 404);
        }
        if ($this->attemptLogin($request)) {
            $user = $this->guard()->user();
            $user->generateToken();

            return Response::json(['data' => $user->toArray()], 201);
        } else {
            return Response::json(['errors' => [
               'error_id'     => 401,
               'error_title'  => 'Authtication Error.',
               'error_message'=> 'Email | Password incorrect',
            ]], 401);
        }
    }
}
