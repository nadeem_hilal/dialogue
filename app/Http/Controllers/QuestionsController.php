<?php

namespace App\Http\Controllers;

use App\Http\Requests\PublishQuestionRequest;
use App\Http\Requests\UpdateQuestionRequest;
use App\Question;
use App\Transformers\QuestionTransformer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class QuestionsController extends ApiController
{
    /**
     * @var App\Transformers\QuestionTransformer
     **/
    protected $questionTransformer;

    public function __construct(QuestionTransformer $questionTransformer)
    {
        $this->questionTransformer = $questionTransformer;
    }

    /**
     *  Lists all Questions.
     *
     * @return Question collection with only unanswered and answered questions.
     */
    public function index()
    {
        $questions = Question::latest()->paginate(6);

        return $this->respond([
            'data'      => $this->questionTransformer->transformCollection($questions->all()),
            'pagination'=> [
                'total_count' => $questions->total(),
                'current_page'=> $questions->currentPage(),
                'total_pages' => ceil($questions->total() / $questions->perPage()),
                'limit'       => $questions->perPage(),
            ],
        ]);
    }

    /**
     *  Show a particular question .
     *
     * @param int $id.
     *
     * @throws Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     *
     * @return Question Object of that particular id.
     */
    public function show($question)
    {
        try {
            return $this->respond(['data' => $this->questionTransformer->transformWithAnswers($question)]);
        } catch (\Exception $e) {
            throw new NotFoundHttpException();
        }
    }

    /**
     * Create question .
     *
     * @param App\Http\Requests\PublishQuestionRequest @request
     *
     * @return mixed
     */
    public function store(PublishQuestionRequest $request)
    {
        Question::publishQuestion($request->all());

        return $this->respondWithSuccess('Question Posted Successuly!');
    }

    /**
     * Delete question.
     *
     * @param App\Question $question
     *
     * @throws Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException
     *
     * @return Response mixed.
     */
    public function delete(Question $question)
    {
        try {
            $this->authorize('delete', $question);
            \App\User::decreaseKarma($question->user, env('DELETE_QUESTION'));
            $question->delete();

            return $this->respondWithSuccess('Question deleted Successuly');
        } catch (\Exception $e) {
            throw new AccessDeniedHttpException();
        }
    }

    /**
     * Update  question.
     *
     * @param  App\Question
     * @param App\Http\Requests\UpdateQuestionRequest $request
     *
     * @throws Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException
     * @throws Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     *
     * @return Response mixed.
     */
    public function update(UpdateQuestionRequest $request, $question)
    {
        try {
            $this->authorize('update', $question);

            $question->update($request->all());

            return $this->respondWithSuccess('Question\'s status updated Successuly!');
        } catch (\Exception $e) {
            throw new AccessDeniedHttpException();
        }
    }

    /**
     * Edit question resource.
     *
     * @param App\Question
     * @param App\Http\Requests\PublishQuestionRequest $request
     *
     * @throws Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException
     * @throws Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     *
     * @return Response mixed.
     */
    public function edit(PublishQuestionRequest $request, $question)
    {
        if ($question->status === 'Answered') {
            return $this->setStatusCode(403)->respondWithError('Cannot edit Answered Question,Talk to admin');
        }

        try {
            $this->authorize('update', $question);
            $question->update($request->all());

            return $this->respondWithSuccess('Question updated Successuly!');
        } catch (\Exception $e) {
            throw new AccessDeniedHttpException();
        }
    }
}
