<?php

use App\Answer;
use App\User;
use Carbon\Carbon;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/
/* \illuminate\Database\Eloquent\Factory $factory */

// $factory->define(Answer::class, function (Faker $faker) {
//     return [
//         'user_id' => function () {
//             return factory(User::class)->create()->id;
//         },
//         'question_id' => function () {
//             return factory('App\Question')->create()->id;
//         },
//         'content'   => $faker->sentence,
//         'posted_at'     => Carbon::now()
//     ];
// });
