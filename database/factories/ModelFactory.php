<?php

use App\Answer;
use App\Question;
use App\User;
use Carbon\Carbon;
use Faker\Generator as Faker;

$factory->define(User::class, function (Faker $faker) {
    return [
            'name'           => $faker->name,
            'user_name'      => $faker->unique()->word,
            'email'          => $faker->unique()->safeEmail,
            'password'       => bcrypt('stranger'),
            'remember_token' => str_random(60),
            'api_token'      => str_random(60),
            'level'          => 1,
            'joined_at'      => Carbon::now(),
            'karma'          => 10,
    ];
});

$factory->define(Question::class, function (Faker $faker) {
    return [
        'title'      => $faker->sentence,
        'content'    => $faker->paragraph,
        'status'     => 'unanswered',
        'posted_at'  => Carbon::now(),
        'deleted_at' => null,
        'user_id'    => function () {
            return factory('App\User')->create()->id;
        },
    ];
});

$factory->define(Answer::class, function (Faker $faker) {
    return [
         'user_id' => function () {
             return factory(User::class)->create()->id;
         },
        'question_id' => function () {
            return factory(Question::class)->create()->id;
        },
        'content'   => $faker->sentence,
        'posted_at' => Carbon::now(),
    ];
});
