<?php

use App\User;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach (range(1, 10) as $index) {
            User::Create([
                'name'           => $faker->name,
                'user_name'      => $faker->unique()->word,
                'email'          => $faker->unique()->safeEmail,
                'password'       => bcrypt('stranger'),
                'remember_token' => str_random(60),
                'api_token'      => str_random(60),
                'level'          => $faker->boolean,
                'joined_at'      => $faker->dateTime(),
                'karma'          => rand(200, 10000),
            ]);
        }
    }
}
