<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    protected $tables = [
        'users', 'questions', 'answers',
    ];
    protected $seeders = [
        'UsersTableSeeder', 'QuestionsTableSeeder', 'AnswersTableSeeder',
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->cleanDatabase();
        foreach ($this->seeders as $seedclass) {
            $this->call($seedclass);
        }
    }

    /**
     * clean database for next seed generation.
     */
    public function cleanDatabase()
    {
        Schema::disableForeignKeyConstraints();
        foreach ($this->tables as $table) {
            DB::table($table)->truncate();
        }
        Schema::enableForeignKeyConstraints();
    }
}
