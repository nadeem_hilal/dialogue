<?php

use App\Answer;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class AnswersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $userIds = DB::table('users')->pluck('id');
        $questionIds = DB::table('questions')->pluck('id');
        foreach (range(1, 40) as $index) {
            Answer::Create([
                'content'         => $faker->paragraph(2),
                'user_id'         => $faker->randomElement($userIds->toArray()),
                'question_id'     => $faker->randomElement($questionIds->toArray()),
                'posted_at'       => $faker->dateTime(),
                'deleted_at'      => null,
               ]);
        }
    }
}
