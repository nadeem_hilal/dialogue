<?php

use App\Question;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class QuestionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $userIds = DB::table('users')->pluck('id');
        foreach (range(1, 30) as $index) {
            Question::Create([
                'title'           => $faker->sentence(2),
                'user_id'         => $faker->randomElement($userIds->toArray()),
                'posted_at'       => $faker->dateTime(),
                'content'         => $faker->paragraph(5),
                'status'          => $faker->randomElement(['answered', 'unanswered']),
                'deleted_at'      => null,
            ]);
        }
    }
}
