<?php

use App\Http\Controllers\ProfileController;
use App\User;

/*
* Route to get question Collection.
* @return Questions collection.
**/
Route::get('1.0/questions', 'QuestionsController@index');
Route::get('1.0/questions/{question}', 'QuestionsController@show');
/*
 * Route for login and registration.
 **/
Route::post('/1.0/login', 'Auth\LoginController@login');
Route::post('/1.0/register', 'Auth\RegisterController@register');
/*
 * Route user profile
 **/
Route::get('/1.0/users/{user}', 'ProfileController@show');

/*
 * Authenticated route groups.
 **/
Route::group(['prefix' => '1.0', 'middleware' =>'auth:api'], function () {

    //question endpoints.
    Route::post('/questions', 'QuestionsController@store');
    Route::put('/questions/{question}', 'QuestionsController@edit');
    Route::patch('/questions/{question}', 'QuestionsController@update');
    Route::delete('/questions/{question}', 'QuestionsController@delete');
    //answer endpoints.
    Route::post('/questions/{question}', 'AnswersController@store');
    Route::patch('/questions/{question}/answers/{answer}', 'AnswersController@edit');
    Route::delete('/questions/{question}/answers/{answer}', 'AnswersController@delete');
    //update profile endpoint.
    Route::put('/users/{user}', 'ProfileController@edit');
});
 //get all users
Route::get('/users', function () {
    return User::all();
});
