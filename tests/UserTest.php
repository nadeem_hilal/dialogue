<?php

namespace Tests;

use App\User;

class UserTest extends TestCase
{
    protected $url;
    protected $user;

    public function setUp()
    {
        parent::setUp();
        $this->url = '/api/1.0/users/';
        $this->user = factory(User::class)->create([
            'user_name' => 'user_nadeem',
            'email'     => 'user@gmail.com',
            'karma'     => 100,
        ]);
    }

    /** @test */
    public function user_karma_is_100()
    {
        $this->get($this->url.$this->user->user_name)
            ->assertJson(['data' => [
                'karma' => 100,
            ]]);
    }

    public function user_karma_is_110()
    {
        $this->get($this->url.$this->user->user_name)
            ->assertJson(['data' => [
                'karma' => 110,
            ]]);
    }
}
