<?php

namespace Tests\Feature\Authentication;

use App\User;
use Tests\TestCase;

class RegistrationTest extends TestCase
{
    /** @test */
    public function a_guest_user_can_register_successfully()
    {
        $user = [
            'user_name' => '@nadeem',
            'email'     => 'nadeem@gmail.com',
            'name'      => 'Nadeem Hilal Wani',
            'password'  => 'stranger',
        ];
        $this->register($user)
            ->assertStatus(201)
            ->assertJson([
                'data'  => [
                    'user_name' => '@nadeem',
                    'email'     => 'nadeem@gmail.com',
                    'name'      => 'Nadeem Hilal Wani',
                ],
            ]);
        $user = User::where('user_name', '@nadeem')->first();
        $this->assertDatabaseHas('users', $user->toArray());
    }

    public function getRegistrationStub($passedData)
    {
        return $passedData;
    }
}
