<?php

namespace Tests\Feature\Authentication;

use App\User;
use Tests\TestCase;

class LoginUserTest extends TestCase
{
    protected $user;

    public function setUp()
    {
        parent::setUp();
        $this->user = factory(User::class)->create([
            'email'   => 'jondoe@hello.com',
            'password'=> bcrypt('hellojhon'),
        ]);
    }

    /** @test */
    public function user_login_is_successfull_when_creds_match()
    {
        $payload = ['email' => 'jondoe@hello.com', 'password' => 'hellojhon'];

        $response = $this->post('/api/1.0/login', $payload);
        $response->assertStatus(201)
            ->assertJson([
                'data'=> [
                    'email' => 'jondoe@hello.com',
                ],
            ]);
    }

    /** @test */
    public function show_401_when_creds_donot_match()
    {
        $payload = ['email' => 'abc@gmail.com', 'password' => 'hellojhon'];

        $response = $this->post('/api/1.0/login', $payload);

        $response->assertStatus(401)
            ->assertJson([
                'errors'=> [
                    'error_id'      => 401,
                    'error_title'   => 'Authtication Error.',
                    'error_message' => 'Email | Password incorrect',
                ],
            ]);
    }

    /** @test */
    public function show_404_email_is_required()
    {
        $payload = ['password'=>'stranger'];

        $response = $this->post('/api/1.0/login', $payload);

        $response->assertStatus(404)
            ->assertJson([
                'errors'=> [
                    'email'=> ['The email field is required.'],
                ],
            ]);
    }

    /** @test */
    public function show_404_email_is_not_valid()
    {
        $payload = ['email'=>'adad@', 'password'=>'stranger'];

        $response = $this->post('/api/1.0/login', $payload);

        $response->assertStatus(404)
        ->assertJson([
            'errors'=> [
                'email'=> ['The email must be a valid email address.'],
                ],
                ]);
    }

    /** @test */
    public function show_404_password_is_required()
    {
        $payload = ['email'=>'nadeem@gmail.com'];

        $response = $this->post('/api/1.0/login', $payload);

        $response->assertStatus(404)
                    ->assertJson([
                        'errors'=> [
                            'password'=> ['The password field is required.'],
                        ],
                    ]);
    }

    /** @test */
    public function show_404_when_password_length_is_less_than_6_characters()
    {
        $payload = ['email' => 'abc@gmail.com', 'password' => 'hello'];

        $response = $this->post('/api/1.0/login', $payload);

        $response->assertStatus(404)
            ->assertJson([
                'errors'=> [
                    'password'=> ['The password must be at least 6 characters.'],
                ],
            ]);
    }
}
