<?php

namespace Tests\Feature\Answer;

use App\Answer;
use Tests\AnswerTest;

class PublishAnswerTest extends AnswerTest
{
    protected $answer;

    public function setUp()
    {
        parent::setUp();

        $this->answer = ['content' => 'This is the answer that user posted.'];
    }

    /** @test */
    public function user_can_publish_answer_on_some_question()
    {
        $url = $this->url.$this->question->id;

        $this->publish($this->answer, $url)
            ->assertStatus(200)
            ->assertJson([
            'data'=> true, ]);
        $this->assertDatabaseHas('answers', $this->answer);
    }
}
