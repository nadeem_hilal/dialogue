<?php

namespace Tests\Feature\Answer;

use App\Answer;
use Tests\AnswerTest;

class EditAnswerTest extends AnswerTest
{
    /** @test */
    public function user_can_edit_his_answer_on_some_question()
    {
        $existingAnswer = factory(Answer::class)->create([
                'user_id'       => $this->user->id,
                'question_id'   => $this->question->id,
                ]);
        $editedAnswer = [
            'content'   => 'This is the edited answer',
        ];
        $this->edits($editedAnswer, $existingAnswer->id)
        ->assertExactJson([
             'data'=> [
                'message' => 'Answer updated Successuly',
                'status'  => 'success',
                ],
        ]);
        $ans = Answer::find($existingAnswer->id)->first();
        $this->assertDatabaseHas('answers', [
            'content'   => 'This is the edited answer',
        ]);
    }
}
