<?php

namespace Tests\Feature\Answer;

use App\Answer;
use Tests\AnswerTest;

class DeleteAnswerTest extends AnswerTest
{
    /** @test */
    public function user_can_delete_his_answer_successfully()
    {
        $answer = factory(Answer::class)->create([
                 'user_id'=> $this->user->id,
            'question_id' => $this->question->id,
            ]);
        $url = $this->url.$this->question->id.'/answers/'.$answer->id;
        $this->deletes($url)
            ->assertStatus(200);
        $this->if_deleted_assert_true($answer->id);
    }
}
