<?php

namespace Tests\Feature\User;

use App\Answer;
use App\Question;
use App\User;
use Tests\UserTest;

class UserKarmaTest extends UserTest
{
    protected $question;

    public function setUp()
    {
        parent::setUp();
        $this->question = factory(Question::class)->create([
                 'user_id' => $this->user->id,
             ]);
    }

    /** @test */
    public function user_will_get_10_karma_points_by_default_when_he_registers()
    {
        $newUser = factory(User::class)->create([
            'user_name' => 'nadeem',
            'email'     => 'nadeem@gmail.com',
        ]);
        $this->assertEquals($newUser->karma, 10);
    }

    /** @test */
    public function show_karma_of_member()
    {
        $headers = ['Accept'=>'application/json'];
        $this->get($this->url.$this->user->user_name)
            ->assertStatus(200)
            ->assertJson(['data' => [
                        'name'      => $this->user->name,
                        'email'     => $this->user->email,
                        'username'  => $this->user->user_name,
                        'karma'     => $this->user->karma,
                    ]]);
    }

    /** @test */
    public function user_karma_increases_when_he_publishes_a_new_question()
    {
        //user karma is 100
        $payload = [
                'title'  => 'New question title by user.',
                'content'=> 'New Question content by user to seee increase his karma.',
        ];
        $this->publish($payload, '/api/1.0/questions', $this->user->api_token)
            ->assertStatus(200);
        $this->user_karma_is_110();
    }

    /** @test */
    public function user_karma_decreases_when_he_deletes_a_question()
    {
        //user karma was 100 and after new question is posted it will be 110.
        $this->user_karma_increases_when_he_publishes_a_new_question();
        //karma should decrease by 10 as he deletes the question.
        $this->deletes('/api/1.0/questions/1', $this->user->api_token)
            ->assertStatus(200);

        $this->user_karma_is_100();
    }

    /** @test */
    public function user_karma_increases_when_new_answer_is_published()
    {
        $payload = [
            'content' => 'This is the New Answer by user to seee increase his karma.',
        ];

        $this->publish($payload, '/api/1.0/questions/'.$this->question->id, $this->user->api_token)
            ->assertStatus(200);

        $this->get($this->url.$this->user->user_name)
         ->assertJson(['data' => [
                'karma' => 105,
            ]]);
    }

    /** @test */
    public function user_karma_decreases_when_answer_is_deleted()
    {
        $answer = factory(Answer::class)->create([
            'question_id'  => $this->question->id,
            'user_id'      => $this->user->id,
        ]);
        $url = '/api/1.0/questions/'.$this->question->id.'/answers/'.$answer->id;
        $this->deletes($url, $this->user->api_token)
            ->assertStatus(200);

        $this->get($this->url.$this->user->user_name)
            ->assertJson(['data' => [
                'karma' => 95,
            ]]);
    }
}
