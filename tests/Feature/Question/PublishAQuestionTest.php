<?php

namespace Tests\Feature\Question;

use App\Question;
use Tests\QuestionTest;

class PublishAQuestionTest extends QuestionTest
{
    /** @test */
    public function publish_question_succesfully()
    {
        $questionForm = [
            'title'  => 'This is the new question',
            'content'=> 'This is the new question content', ];
        $status = $this->publish($questionForm);

        $question = Question::where('title', 'This is the new question')->first();
        $response = $this->get($this->url.$question->id);

        $response->assertStatus(200)
            ->assertJson([
                'data'=> [
                    'title' => 'This is the new question',
                    'body'  => 'This is the new question content',
                ],
            ]);
    }

    /** @test */
    public function show_422_when_validation_fails_on_publishing_new_question()
    {
        $questionForm = [];
        $response = $this->publish($questionForm);
        $response->assertStatus(422)
            ->assertJson([
                'errors' => [
                        'title'      => ['The title field is required.'],
                        'content'    => ['The content field is required.'],
                ],
            ]);
    }
}
