<?php

namespace Tests\Feature\Question;

use App\Question;
use App\User;
use DB;
use Tests\QuestionTest;

class ViewQuestionsTest extends QuestionTest
{
    /** @test */
    public function view_questions_on_homepage()
    {
        $users = factory(User::class, 2)->create();
        $userIds = DB::table('users')->pluck('id');
        $questions = factory(Question::class, 3)->create([
            'title'   => $this->fake->sentence(2),
            'content' => $this->fake->paragraph(5),
            'user_id' => $this->fake->randomElement($userIds->toArray()),
        ]);
        $response = $this->get($this->url);

        $response->assertStatus(200)
            ->assertJson([
                'data'      => true,
                'pagination'=> [
                ],
            ]);
    }

    /** @test */
    public function view_a_particular_question()
    {
        $postedQuestion = factory(Question::class)->create([
            'title'   => 'This is the new laravel question',
            'content' => 'this is the content of the new laravel question',
            'user_id' => $this->user->id,
        ]);

        $response = $this->get($this->url.$postedQuestion->id);

        $this->assertEquals($postedQuestion->title, 'This is the new laravel question');
        $response->assertStatus(200);
    }

    /** @test */
    public function the_latest_question_is_shown_at_top()
    {
        $users = factory(User::class, 2)->create();
        $userIds = DB::table('users')->pluck('id');
        $questions = factory(Question::class, 3)->create([
            'title'   => $this->fake->sentence(2),
            'content' => $this->fake->paragraph(5),
            'user_id' => $this->fake->randomElement($userIds->toArray()),
        ]);
        $latestQuestion = factory(Question::class)->create([
            'title'   => 'This is the latest question',
            'content' => $this->fake->paragraph(5),
            'user_id' => 1,
        ]);

        $response = $this->get($this->url);

        $response->assertStatus(200)
            ->assertJson([
                'data'=> [
                    0 => [
                        'id'    => $latestQuestion->id,
                        'title' => $latestQuestion->title,
                    ],
                ],
                'pagination'=> true,
            ]);
    }

    /** @test */
    public function show_404_when_wrong_question_id_is_routed()
    {
        $postedQuestion = factory(Question::class)->create([
                'title'   => 'This is the new laravel question',
                'content' => 'this is the content of the new laravel question',
                'user_id' => $this->user->id,
            ]);

        $response = $this->get('/api/1.0/questions/x');

        $response->assertStatus(404)
            ->assertJson([
                'error'        => 'not_found',
                'error_message'=> 'Please check the URL you submitted.',

            ]);
    }
}
