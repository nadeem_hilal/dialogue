<?php

namespace Tests\Feature\Question;

use App\Question;
use App\User;
use Tests\QuestionTest;

class EditQuestionTest extends QuestionTest
{
    /** @test */
    public function user_can_edits_his_question_successfully()
    {
        $withEditedQuestion = [
            'title'   => 'User edited title of question',
            'content' => 'User edited content of question',
         ];
        $url = '/api/1.0/questions/'.$this->question->id;
        $this->edit($withEditedQuestion, $url, $this->user->api_token)
            ->assertStatus(200);

        $this->get($url)
         ->assertJson([
             'data' => [
                 'title'  => 'User edited title of question',
                 'body'   => 'User edited content of question',
             ],
         ]);
    }
}
