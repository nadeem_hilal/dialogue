<?php

namespace Tests\Feature\Question;

use App\Question;
use Tests\QuestionTest;

class UpdateQuestionTest extends QuestionTest
{
    protected $question;

    public function setUp()
    {
        parent::setUp();

        $question = factory(Question::class)->create([
            'title'   => 'This is question title ',
            'content' => 'This is question content',
            'user_id' => $this->user->id,
            'status'  => 'unanswered',
        ]);
    }

    /** @test */
    public function user_can_update_his_question()
    {
        $updateStatus = ['status' => 'answered'];
        $this->updatesQuestion($this->question->id, $updateStatus)
            ->assertStatus(200);

        $this->get($this->url.$this->question->id)
        ->assertJson(['data' => [
                    'id'     => $this->question->id,
                    'status' => 'Answered',
                ],
            ]);
    }
}
