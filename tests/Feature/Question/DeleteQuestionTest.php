<?php

namespace Tests\Feature\Question;

use Tests\QuestionTest;

class DeleteQuestionTest extends QuestionTest
{
    /** @test */
    public function user_can_delete_his_question_successfully()
    {
        $url = '/api/1.0/questions/'.$this->question->id;
        $headers = ['Authorization' => 'Bearer '.$this->user->api_token, 'Accept' =>'application/json'];
        $this->delete($url, [], $headers)
          ->assertStatus(200);

        $this->assertDatabaseMissing('questions', $this->question->toArray());

        $this->get($url)->assertStatus(404)
            ->assertJson([
                'error'        => 'not_found',
                'error_message'=> 'Please check the URL you submitted.',
            ]);
    }
}
