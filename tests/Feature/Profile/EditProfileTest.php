<?php

namespace Tests\Feature\Profile;

use Tests\ProfileTest;

class EditProfileTest extends ProfileTest
{
    /** @test */
    public function user_can_edit_his_profile_successfully()
    {
        $url = 'api/1.0/users/'.$this->user->user_name;
        $editForm = ['name'=> 'Nadeem Hilal Wani'];

        $this->user()->edit($editForm, $url);

        $this->get($this->url.$this->user->user_name)
            ->assertStatus(200)
            ->assertJson([
                'data' => [
                    'id'    => $this->user->id,
                    'name'  => 'Nadeem Hilal Wani',
                ],
            ]);
    }
}
