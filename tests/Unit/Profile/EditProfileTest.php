<?php

namespace Tests\Unit\Profile;

use App\User;
use Tests\ProfileTest;

class EditProfileTest extends ProfileTest
{
    /** @test */
    public function show_403_when_user_attempts_to_edit_other_user_profile()
    {
        $user2 = factory(User::class)->create();
        $uri = '/api/1.0/users/'.$this->user->user_name;
        $payload = ['name'=>'Nadeem'];  //name must be atleast 4 chars long

        $this->edit($payload, $uri, $user2->api_token)
            ->assertStatus(403);
    }

    /** @test */
    public function show_404_when_username_notfound()
    {
        $uri = '/api/1.0/users/xxxxxxx';
        $payload = ['name'=>'Nadeem'];  //name must be atleast 4 chars long

        $this->edit($payload, $uri)
            ->assertStatus(404);
    }

    /** @test */
    public function throws_400_when_user_edit_profile_and_validation_fails()
    {
        $editForm = [];  //name must be atleast 4 chars long

        $this->edit($editForm, $this->url.$this->user->user_name)
            ->assertStatus(422)
            ->assertJson([
            'errors' => ['name' => ['The name field is required.']],
         ]);
    }
}
