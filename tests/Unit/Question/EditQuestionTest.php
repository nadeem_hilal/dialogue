<?php

namespace Tests\Unit\Question;

use App\Question;
use App\User;
use Tests\QuestionTest;

class EditQuestionTest extends QuestionTest
{
    protected $question;

    public function setUp()
    {
        parent::setUp();
        $this->question = factory(Question::class)->create([
                    'title'     => 'This is the new laravel question',
                    'content'   => 'this is the content of the new laravel question',
                    'user_id'   => $this->user->id,
            ]);
    }

    /** @test */
    public function show_403_when_user_attempts_to_edit_answered_question()
    {
        $answeredQuestion = factory(Question::class)->create([
                'user_id'   => $this->user->id,
                'status'    => 'answered',
            ]);
        $editingAnsweredQuestion = [
            'title'  => 'User  is editing title of an answered question',
            'content'=> 'User  is editing content of an answered question',
         ];
        $this->editsQuestion($answeredQuestion->id, $editingAnsweredQuestion)
            ->assertStatus(403)
            ->assertJson([
                'errors' => 'Cannot edit Answered Question,Talk to admin',
            ]);
    }

    /** @test */
    public function show_404_when_user_attempts_to_edit_question_not_in_database()
    {
        $withEditedQuestion = [
            'title'   => 'Other user edited title of question',
            'content' => 'Other user edited content of question',
         ];

        $this->user()
            ->editsQuestion('x', $withEditedQuestion)
            ->assertStatus(404)
            ->assertJson([
                'error_message' => 'Please check the URL you submitted.',
            ]);
    }

    /** @test */
    public function show_400_when_user_attempts_to_edit_his_question_and_validation_fails()
    {
        $withEditedQuestion = [
        ];
        $this->user()
            ->editsQuestion($this->question->id, $withEditedQuestion)
            ->assertStatus(422)
            ->assertJson([
                'errors' => [
                    'title'     => ['The title field is required.'],
                    'content'   => ['The content field is required.'],
                ],
            ]);
    }

    /** @test */
    public function show_403_when_user_attempts_to_edit_other_users_question()
    {
        $byOtherUser = factory(User::class)->create();
        $withEditedQuestion = [
            'title'   => 'Other user edited title of question',
            'content' => 'Other user edited content of question',
         ];

        $this->editsQuestion($this->question->id, $withEditedQuestion, $byOtherUser->api_token)
            ->assertStatus(403)
            ->assertJson([
                'error'            => 'un-authorized',
                'error_message'    => 'You are un-authorized for this activity.',
            ]);
    }
}
