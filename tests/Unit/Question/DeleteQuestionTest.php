<?php

namespace Tests\Unit\Question;

use Tests\QuestionTest;

class DeleteQuestionTest extends QuestionTest
{
    /** @test */
    public function show_404_when_user_deletes_question_not_in_database()
    {
        $url = '/api/1.0/questions/5';
        $headers = ['Authorization' => 'Bearer '.$this->user->api_token, 'Accept' => 'application/json'];
        $this->delete($url, [], $headers)
            ->assertStatus(404)
            ->assertJson([
                'error'         => 'not_found',
                'error_message' => 'Please check the URL you submitted.',
            ]);
    }

    /** @test */
    public function show_403_when_user_attempts_to_delete_other_users_question()
    {
        $user2 = factory(\App\User::class)->create();
        $url = '/api/1.0/questions/'.$this->question->id;
        $headers = ['Authorization' => 'Bearer '.$user2->api_token, 'Accept' =>'application/json'];

        $this->delete($url, [], $headers)
            ->assertStatus(403)
            ->assertJson([
                'error'         => 'un-authorized',
                'error_message' => 'You are un-authorized for this activity.',
            ]);
    }
}
