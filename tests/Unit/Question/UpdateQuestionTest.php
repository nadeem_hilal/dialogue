<?php

namespace Tests\Unit\Question;

use App\Question;
use App\User;
use Tests\QuestionTest;

class UpdateQuestionTest extends QuestionTest
{
    protected $question;

    public function setUp()
    {
        parent::setUp();

        $question = factory(Question::class)->create([
            'user_id' => $this->user->id,
            'status'  => 'unanswered',
        ]);
    }

    /** @test */
    public function show_400_when_user_updates_his_question_and_validation_fails()
    {
        $updateStatus = [];

        $this->updatesQuestion($this->question->id, $updateStatus)
            ->assertStatus(422)
            ->assertJson(['errors' =>true]);
    }

    /** @test */
    public function show_404_when_user_update_question_not_in_database()
    {
        $updateStatus = ['status' => 'answered'];
        $this->updatesQuestion('x', $updateStatus)
            ->assertStatus(404);
    }

    /** @test */
    public function show_403_when_user_attempts_to_update_other_users_question()
    {
        $user2 = factory(User::class)->create();

        $updateStatus = ['status' => 'answered'];

        $this->updatesQuestion($this->question->id, $updateStatus, $user2->api_token)
            ->assertStatus(403);
    }
}
