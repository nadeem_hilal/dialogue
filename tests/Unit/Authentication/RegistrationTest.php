<?php

namespace Tests\Unit\Authentication;

use App\User;
use Tests\TestCase;

class RegistrationTest extends TestCase
{
    /** @test */
    public function show_400_when_all_validation_fails_as_guest_user_attempts_to_register()
    {
        $user = [];
        $this->register($user)
            ->assertStatus(400)
            ->assertJson([
                'errors' => true,
            ]);
    }

    /** @test */
    public function user_name_required()
    {
        $user = [
            'email'    => 'nadeem@gmail.com',
            'name'     => 'Nadeem Hilal Wani',
            'password' => 'stranger',
        ];
        $this->register($user)
            ->assertStatus(400)
            ->assertJson([
                'errors' => [
                    'user_name' => ['The user name field is required.'],
                ],
            ]);
    }

    /** @test */
    public function user_name_already_taken()
    {
        $user1 = factory(User::class)->create(['user_name' => '@nadeem']);
        $user = [
            'user_name'=> '@nadeem',
            'email'    => 'nadeem@gmail.com',
            'name'     => 'Nadeem Hilal Wani',
            'password' => 'stranger',
        ];
        $this->register($user)
            ->assertStatus(400)
            ->assertJson([
                'errors' => [
                    'user_name' => ['The user name has already been taken.'],
                ],
            ]);
    }

    /** @test */
    public function user_name_not_string()
    {
        $user = [
            'user_name' => 121212121,
            'email'     => 'nadeem@gmail.com',
            'name'      => 'Nadeem Hilal Wani',
            'password'  => 'stranger',
        ];
        $this->register($user)
            ->assertStatus(400)
            ->assertJson([
                'errors' => [
                    'user_name' => ['The user name must be a string.'],
                ],
            ]);
    }

    /** @test */
    public function user_name_must_be_atleast_5_character_string()
    {
        $user = [
            'user_name' => '@nad',
            'email'     => 'nadeem@gmail.com',
            'name'      => 'Nadeem Hilal Wani',
            'password'  => 'stranger',
        ];
        $this->register($user)
            ->assertStatus(400)
            ->assertJson([
                'errors' => [
                    'user_name' => ['The user name must be at least 5 characters.'],
                ],
            ]);
    }

    /** @test */
    public function password_is_required()
    {
        $user = [
            'user_name' => '@nadeem',
            'email'     => 'nadeem@gmail.com',
            'name'      => 'Nadeem Hilal Wani',
            // 'password' => 'stranger'
        ];
        $this->register($user)
            ->assertStatus(400)
            ->assertJson([
                'errors' => [
                    'password' => ['The password field is required.'],
                ],
            ]);
    }

    /** @test */
    public function password_not_string()
    {
        $user = [
            'user_name' => '@nadeem',
            'email'     => 'nadeem@gmail.com',
            'name'      => 'Nadeem Hilal Wani',
            'password'  => 111,
        ];
        $this->register($user)
            ->assertStatus(400)
            ->assertJson([
                'errors' => [
                    'password' => ['The password must be a string.'],
                ],
            ]);
    }

    /** @test */
    public function password_must_be_a_string_with_atleast_6_characters()
    {
        $user = [
            'user_name' => '@nadeem',
            'email'     => 'nadeem@gmail.com',
            'name'      => 'Nadeem Hilal Wani',
            'password'  => 'aaaa',
        ];
        $this->register($user)
            ->assertStatus(400)
            ->assertJson([
                'errors' => [
                    'password' => ['The password must be at least 6 characters.'],
                ],
            ]);
    }

    /** @test */
    public function email_is_required()
    {
        $user = [
            'user_name' => '@nadeem',
           // 'email' => 'nadeem@gmail.com',
            'name'     => 'Nadeem Hilal Wani',
            'password' => 'stranger',
        ];
        $this->register($user)
            ->assertStatus(400)
            ->assertJson([
                'errors' => [
                    'email' => ['The email field is required.'],
                ],
            ]);
    }

    /** @test */
    public function email_is_must_be_valid_email()
    {
        $user = [
            'user_name' => '@nadeem',
            'email'     => 'nadeemgmail.com',
            'name'      => 'Nadeem Hilal Wani',
            'password'  => 'stranger',
        ];
        $this->register($user)
            ->assertStatus(400)
            ->assertJson([
                'errors' => [
                    'email' => ['The email must be a valid email address.'],
                ],
            ]);
    }

    /** @test */
    public function email_already_exists()
    {
        $user1 = factory(User::class)->create(['email' => 'nadeem@gmail.com']);
        $user = [
            'user_name' => '@nadeem',
            'email'     => 'nadeem@gmail.com',
            'name'      => 'Nadeem Hilal Wani',
            'password'  => 'stranger',
        ];
        $this->register($user)
            ->assertStatus(400)
            ->assertJson([
                'errors' => [
                    'email' => ['The email has already been taken.'],
                ],
            ]);
    }

    /** @test */
    public function name_is_required()
    {
        $user = [
            'user_name' => '@nadeem',
            'email'     => 'nadeem@gmail.com',
           // 'name' => 'Nadeem Hilal Wani',
            'password' => 'stranger',
        ];
        $this->register($user)
            ->assertStatus(400)
            ->assertJson([
                'errors' => [
                    'name' => ['The name field is required.'],
                ],
            ]);
    }

    /** @test */
    public function name_is_must_be_string()
    {
        $user = [
            'user_name' => '@nadeem',
            'email'     => 'nadeem@gmail.com',
            'name'      => 32333,
            'password'  => 'stranger',
        ];
        $this->register($user)
            ->assertStatus(400)
            ->assertJson([
                'errors' => [
                    'name' => ['The name must be a string.'],
                ],
            ]);
    }

    /** @test */
    public function name_must_not_exceed_more_then_20_characters()
    {
        $user = [
            'user_name' => '@nadeem',
            'email'     => 'nadeem@gmail.com',
            'name'      => 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
            'password'  => 'stranger',
        ];
        $this->register($user)
            ->assertStatus(400)
            ->assertJson([
                'errors' => [
                    'name' => ['The name may not be greater than 20 characters.'],
                ],
            ]);
    }
}
