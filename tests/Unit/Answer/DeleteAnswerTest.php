<?php

namespace Tests\Unit\Answer;

use App\Answer;
use App\User;
use Illuminate\Support\Carbon;
use Tests\AnswerTest;

class DeleteAnswerTest extends AnswerTest
{
    protected $deletedAnswer;

    public function setUp()
    {
        parent::setUp();
        $this->deletedAnswer = factory(Answer::class)->create([
            'user_id'     => $this->user->id,
            'question_id' => $this->question->id,
            'deleted_at'  => Carbon::now(),
        ]);
    }

    /** @test */
    public function show_404_when_user_attempts_to_delete_a_deleted_answer()
    {
        $this->if_deleted_assert_true($this->deletedAnswer->id);

        $this->attempt_to_delete_a_deleted_answer($this->deletedAnswer)
            ->assertStatus(404);
    }

    /** @test */
    public function show_404_when_user_attempts_to_read_a_deleted_answer()
    {
        $this->if_deleted_assert_true($this->deletedAnswer->id);

        $this->attempt_to_read_a_deleted_answer($this->deletedAnswer)
            ->assertStatus(404);
    }

    /** @test */
    public function show_403_when_user_attempts_to_delete_other_users_answer()
    {
        $answer = factory(Answer::class)->create([
            'user_id'    => $this->user->id,
            'question_id'=> $this->question->id,
        ]);
        $user2 = factory(User::class)->create();
        $url = $this->url.$this->question->id.'/answers/'.$answer->id;
        $this->deletes($url, $user2->api_token)
            ->assertStatus(403)
            ->assertJson([
                'error_message' => 'You are un-authorized for this activity.',
            ]);
    }
}
