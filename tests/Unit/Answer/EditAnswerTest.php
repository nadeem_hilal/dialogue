<?php

namespace Tests\Unit\Answer;

use App\Answer;
use App\User;
use Tests\AnswerTest;

class EditAnswerTest extends AnswerTest
{
    protected $answer;

    public function setUp()
    {
        parent::setUp();
        $this->answer = factory(Answer::class)->create([
                'content'   => 'This was the answer posted.',
        ]);
    }

    /** @test */
    public function show_422_when_user_attempts_to_edit_his_answer_on_some_question_and_validation_fails()
    {
        $editedAnswer = [];
        $this->edits($editedAnswer, $this->answer->id)
            ->assertStatus(422)
            ->assertJson([
                 'errors'=> [
                    'content' => ['The content field is required.'],
                    ],
            ]);
    }

    /** @test */
    public function show_403_when_user_attempts_to_edit_other_users_answer_on_some_question()
    {
        $editedAnswer = [
            'content'   => 'This was the answer posted.',
        ];
        $user2 = factory(User::class)->create();
        $this->edits($editedAnswer, $this->answer->id, $user2->api_token)
            ->assertStatus(403)
            ->assertJson([
                 'error_message'=> 'You are un-authorized for this activity.',
            ]);
    }
}
