<?php

namespace Tests\Unit\Answer;

use Tests\AnswerTest;

class PublishAnswerTest extends AnswerTest
{
    /** @test */
    public function show_422_when_user_attempts_to_publish_answer_and_validation_fails()
    {
        $answer = [];
        $url = $this->url.$this->question->id;

        $this->publish($answer, $url)
            ->assertStatus(422)
            ->assertJson([
                 'errors'=> [
                    'content'   => ['The content field is required.'],
            ],
            ]);
    }

    /** @test */
    public function show_404_when_user_attempts_to_publish_answer_on_question_not_found_in_database()
    {
        $answer = ['content' => 'This answer on wrong question'];
        $url = $this->url.'111';
        $this->publish($answer, $url)
            ->assertStatus(404)
            ->assertJson([
                 'error'        => 'not_found',
                 'error_message'=> 'Please check the URL you submitted.',
            ]);
    }
}
