<?php

namespace Tests\Unit\User;

use App\User;
use Tests\UserTest;

class UserKarmaTest extends UserTest
{
    /** @test */
    public function increase_karma()
    {
        $user = factory(User::class)->create([
            'user_name' => '@abcss',
            'email'     => 'adad@gmail.com',
            'karma'     => 100,
        ]);
        User::increaseKarma($user, env('NEW_ANSWER'));
        $this->assertEquals($user->karma, 105);
    }

    /** @test */
    public function decrease_karma()
    {
        $user = factory(User::class)->create([
            'user_name' => '@nadeem',
            'email'     => 'nadeem@gmail.com',
            'karma'     => 100,
        ]);
        User::decreaseKarma($user, env('DELETE_ANSWER'));
        $this->assertEquals($user->karma, 95);
    }
}
