<?php

namespace Tests;

use App\Answer;
use App\Question;

class AnswerTest extends TestCase
{
    protected $question;
    protected $url;

    public function setUp()
    {
        parent::setUp();
        $this->url = '/api/1.0/questions/';
        $this->question = factory(Question::class)->create([
            'user_id'   => $this->user->id,
        ]);
    }

    public function edits($payload, $id, $token = null)
    {
        $url = $this->url.$this->question->id.'/answers/'.$id;
        $token = $token ?: $this->user->api_token;
        $headers = ['Authorization' => 'Bearer '.$token, 'Accept'=>'application/json'];

        return $this->patch($url, $payload, $headers);
    }

    public function if_deleted_assert_true($id)
    {
        $isDeleted = Answer::withTrashed()->where('id', $id)->get();

        return ($isDeleted === null) ?: $this->assertTrue(true);
    }

    public function attempt_to_delete_a_deleted_answer($answer)
    {
        return  $this->deletes($answer);
    }

    public function attempt_to_read_a_deleted_answer($answer)
    {
        return $this->deletes($answer);
    }
}
