<?php

namespace Tests;

use App\Question;
use App\User;

class QuestionTest extends TestCase
{
    protected $question;
    protected $url;

    public function setUp()
    {
        parent::setUp();
        $this->url = '/api/1.0/questions/';
        $this->question = factory(Question::class)->create([
                'title'  => 'The user question title.',
                'content'=> 'This is the content of the question',
                'user_id'=> $this->user->id,
            ]);
    }

    public function editsQuestion($id, $question, $token = null)
    {
        $url = $this->url.$id;
        $token = $token ?: $this->user->api_token;
        $headers = ['Authorization' => 'Bearer '.$token, 'Accept' => 'application/json'];

        return $this->put($url, $question, $headers);
    }

    public function updatesQuestion($id, array $updateStatus, $token = null)
    {
        $url = $this->url.$id;
        $token = $token ?: $this->user->api_token;
        $headers = ['Authorization' => 'Bearer '.$token, 'Accept'=>'application/json'];

        return $this->patch($url, $updateStatus, $headers);
    }
}
