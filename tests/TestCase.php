<?php

namespace Tests;

use App\User;
use Faker\Factory as Faker;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Support\Facades\Artisan;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication, DatabaseMigrations;
    protected $faker;
    protected $user;

    public function setUp()
    {
        parent::setUp();
        //Artisan::call('migrate');

        $this->fake = Faker::create();
        $this->user = factory(User::class)->create();
    }

    public function user()
    {
        return $this;
    }

    public function register(array $user)
    {
        $url = '/api/1.0/register';

        return $this->post($url, $user);
    }

    public function make($model, array $data)
    {
        return factory($model)->create($data);
    }

    public function publish($payload, $url = null, $token = null)
    {
        $url = $url ?: $this->url;
        $token = $token ?: $this->user->api_token;
        $headers = ['Authorization' => 'Bearer '.$token,  'Accept' =>'application/json'];

        return $this->post($url, $payload, $headers);
    }

    public function deletes($url, $token = null)
    {
        $token = $token ?: $this->user->api_token;
        $headers = ['Authorization' => 'Bearer '.$token,  'Accept' =>'application/json'];

        return $this->delete($url, [], $headers);
    }

    public function edit($payload, $url, $token = null)
    {
        $token = $token ?: $this->user->api_token;
        $headers = ['Authorization' => 'Bearer '.$token, 'Accept' =>'application/json'];

        return $this->put($url, $payload, $headers);
    }
}
